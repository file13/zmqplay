cmake_minimum_required( VERSION 3.10 )
project( zeromqplay VERSION 1.0 )

include( ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake )
conan_basic_setup()

#SET(GCC_COVERAGE_COMPILE_FLAGS "-D_FORTIFY_SOURCE=2 -fpie -Wl,-pie -fpic -shared -g3 -O2 -Wall -Werror -std=c17 -pedantic")
#SET(GCC_COVERAGE_LINK_FLAGS    "-lgcov")

# On GCC, we have to compile/link with g++, force C++ linker.                      
set_source_files_properties(hwserver.c PROPERTIES LANGUAGE CXX )
set_source_files_properties(ironhouse.c PROPERTIES LANGUAGE CXX )

add_executable( hwserver hwserver.c )
add_executable( ironhouse ironhouse.c )
target_link_libraries( hwserver ${CONAN_LIBS} )
target_link_libraries( ironhouse ${CONAN_LIBS} )

# Or if static
#target_link_libraries( hwserver ${CONAN_LIBS} -static )
#target_link_libraries( ironhouse ${CONAN_LIBS} -static )
