# zmqplay

This is an example of a portable cross-platform C/C++ program built
with conan and cmake using ZeroMQ.

Conan fetches the dependencies and cmake builds it.

## Build

You'll need to have conan and cmake installed.

To install conan, you need Python, then do:

```
pip install conan
```

or to upgrade

```
pip install -U conan
```

Then, once your .conan profile is setup (see below), simply run

On Windows: `make_vc.bat`

Unix: `make`

You may need to tweak either of these to customize the settings.

## Manual build.

See conan getting started:

https://docs.conan.io/en/latest/getting_started.html

Setup profile in .conan:

Windows:
```
[build_requires]
[settings]
    os=Windows
    arch=x86_64
    compiler=Visual Studio
    compiler.version=15
    build_type=Release
[options]
[env]
```

Linux:
```
[settings]
arch=x86_64
arch_build=x86_64
build_type=Release
compiler=gcc
compiler.libcxx=libstdc++
compiler.version=9
os=Linux
os_build=Linux
[options]
[build_requires]
[env]
```

# Conan Cheatsheet

Find dependencies:

```
conan search zeromq --remote=conan-center
```

From inside build dir

```
conan install ..
```

To auto build missing:

```
conan install .. --build=missing
```

or to force build from sources:

```
conan install .. --build zeromq --build libsodium
```

Once conan is setup, recompile with cmake from build dir:

Windows:
```
cmake .. -G "Visual Studio 15" -A x64
cmake --build . --config Release
```

Linux:
```
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build .
```
