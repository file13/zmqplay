@echo off
cd build
REM conan install ..
conan install .. --build=missing
REM conan install .. --build zeromq --build libsodium
cmake .. -G "Visual Studio 15" -A x64
cmake --build . --config Release
cd ..
