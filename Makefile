# This must be the first this in Makefile.common
TOP := $(dir $(lastword $(MAKEFILE_LIST)))

all:
	#cd build; conan install ..; conan install .. --build zeromq --build libsodium;
	cd build; conan install .. --build=missing ; \
	cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release; cmake --build .

